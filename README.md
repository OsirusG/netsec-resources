
# NetSec Resources

## Courses
 
- [Sans Cyber Aces](https://www.cyberaces.org/courses.html)
- [CompTIA Network+](https://www.comptia.org/certifications/network)
    - [Training Course Playlist N10-007](https://www.youtube.com/playlist?list=PLG49S3nxzAnmpdmX7RoTOyuNJQAb-r-gd)
    - [Training Course Material N10-007](https://www.professormesser.com/network-plus/n10-007/n10-007-training-course/)
- [CompTIA Security+](https://www.comptia.org/certifications/security)
    - [Training Course Playlist SY0-601](https://www.youtube.com/playlist?list=PLG49S3nxzAnkL2ulFS3132mOVKuzzBxA8)
    - [Training Course Material SY0-601](https://www.professormesser.com/security-plus/sy0-601/sy0-601-video/sy0-601-comptia-security-plus-course/)
    - [Training Course Playlist SY0-401](https://www.youtube.com/playlist?list=PLG49S3nxzAnlhMM1KV5ST1qi3kI87hMpY)

## Articles & Additional Resources

- https://teamciso.com/2020/04/free-training.html
- https://systemoverlord.com/2017/09/18/getting-started-in-offensive-security.html
- https://s3ctur.wordpress.com/2017/06/19/breaking-into-infosec-a-beginners-curriculum/
- https://www.freecodecamp.org/news/so-you-want-to-work-in-security-bc6c10157d23/
